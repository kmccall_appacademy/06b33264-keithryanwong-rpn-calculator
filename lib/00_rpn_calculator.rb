class RPNCalculator
  # TODO: your code goes here!
  def initialize
    @stack = []
    @num_count = 0
    @op_count = 0
  end

  def value
    i = 0
    while i < @stack.length
      if @stack[i] =~ /\D/
        op = @stack[i]
        i -= 2
        val1, val2 = @stack[i..(i + 1)]
        @stack[i] = val1.method(op).call(val2)
        @stack.delete_at(i + 1)
        @stack.delete_at(i + 1)
      end
      i += 1
    end
    @stack[i - 1]
  end

  def push(num)
    @num_count += 1
    @stack.push(num.to_f)
  end

  def plus
    @op_count += 1
    raise 'calculator is empty' if @num_count - @op_count < 1

    @stack.push(:+)
  end

  def minus
    @op_count += 1
    raise 'calculator is empty' if @num_count - @op_count < 1

    @stack.push(:-)
  end

  def divide
    @op_count += 1
    raise 'calculator is empty' if @num_count - @op_count < 1

    @stack.push(:/)
  end

  def times
    @op_count += 1
    raise 'calculator is empty' if @num_count - @op_count < 1

    @stack.push(:*)
  end

  def push_op(op)
    @op_count += 1
    raise 'calculator is empty' if @num_count - @op_count < 1

    @stack.push(op.to_sym)
  end

  def evaluate(str)
    str.split(' ').each do |el|
      self.push(el) if el =~ /\d/
      self.push_op(el) if el =~ /\D/
    end
    self.value
  end

  def tokens(str)
    token_arr = []
    str.split(' ').map do |el|
      token_arr.push(el.to_f) if el =~ /\d/
      token_arr.push(el.to_sym) if el =~ /\D/
    end
    token_arr
  end
end
